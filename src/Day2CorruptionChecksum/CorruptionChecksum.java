package Day2CorruptionChecksum;

import java.io.*;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Day 2: Corruption Checksum
 * Solver for puzzle form The first second. Where The main quest is to calculate checksum from spreadsheet by given rules.
 */

public class CorruptionChecksum {
    /**
     * Loads lines from input file and induce method that calculate line checksum. which are summed up.
     * @param fileName name of the file with spreadsheet
     */
    public void  calculateChecksumPart1(String fileName){
        Integer checkSum = 0;
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            String line;
            while (true){
                line = reader.readLine();
                if (line == null) break;
                checkSum += getMaxAndMinDifference(covertLineToList(line));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(checkSum);
    }

    /**
     * Take String line and spit if into separate numbers. Next convert them to Integer and add to the list
     * @param line  line of numbers from input file
     * @return list consisting of numbers from line
     */
    private List<Integer> covertLineToList(String line){
        List<Integer> lineNumbers = new LinkedList<>();

        String[] separatesLine = line.split("\t");

        for(String number : separatesLine){
            lineNumbers.add(Integer.parseInt(number));
        }

        return lineNumbers;
    }

    /**
     * Find max and min value in list suing stream.
     * @param lineNumbers List of numbers
     * @return difference between max and min value in list
     */
    private Integer getMaxAndMinDifference(List<Integer> lineNumbers){
        Integer max = lineNumbers.stream().max(Comparator.comparing(Integer::valueOf)).get();

        Integer min = lineNumbers.stream().min(Comparator.comparing(Integer::valueOf)).get();

        return Math.abs(max - min);
    }

    /**
     * Loads lines from input file and induce method that calculate line checksum. which are summed up.
     * @param fileName name of the file with spreadsheet
     */

    public void  calculateChecksumPart2(String fileName){
        Integer checkSum = 0;
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            String line;
            while (true){
                line = reader.readLine();
                if (line == null) break;
                System.out.println(getEvenlyDivisibleValues(covertLineToList(line)));
                checkSum += getEvenlyDivisibleValues(covertLineToList(line));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(checkSum);
    }

    /**
     * Return division result where the result of the division operation is a whole number
     * @param lineNumbers List of numbers
     * @return difference
     */

    private Integer getEvenlyDivisibleValues(List<Integer> lineNumbers){
        for(Integer number1 : lineNumbers){
            for(Integer number2 : lineNumbers) {
                if(number1 != number2 && number1%number2 == 0) return number1/number2;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        CorruptionChecksum corruptionChecksum = new CorruptionChecksum();
        System.out.print("calculateChecksumPart1: ");
        corruptionChecksum.calculateChecksumPart1("Day2DataPart1");
        System.out.print("calculateChecksumPart2: ");
        corruptionChecksum.calculateChecksumPart2("Day2DataPart2");
    }


}
