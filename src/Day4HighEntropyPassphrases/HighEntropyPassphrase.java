package Day4HighEntropyPassphrases;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class HighEntropyPassphrase {
    public boolean validate(String passphrase) {
        if(passphrase == null) return false;

        String[] passphraseSplit = passphrase.split(" ");

        for(String word1 : passphraseSplit){
            for(String word2 : passphraseSplit) {
                if(word1 != word2){
                    char[] word1Char = word1.toCharArray();
                    char[] word2Char = word2.toCharArray();

                    Arrays.sort(word1Char);
                    Arrays.sort(word2Char);
                    if(Arrays.equals(word1Char, word2Char)) return false;
                }
            }
        }
        return true;
    }

    private int countValidInFile(String fileName){
        int quantity = 0;
        String line;
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))){
        while(true){
            line = reader.readLine();
            if(line == null) break;
            if(validate(line)) quantity++;
        }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return quantity;
    }


    public static void main(String[] args) {
        HighEntropyPassphrase highEntropyPassphrase = new HighEntropyPassphrase();
        System.out.println(highEntropyPassphrase.countValidInFile("Day4DataPart1"));
    }
}
