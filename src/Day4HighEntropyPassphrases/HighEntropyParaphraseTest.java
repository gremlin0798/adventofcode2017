package Day4HighEntropyPassphrases;

import org.junit.Test;

import static org.junit.Assert.*;

@SuppressWarnings("SpellCheckingInspection")
public class HighEntropyParaphraseTest {
    @Test
    public void shouldValidPassphrase(){
        HighEntropyPassphrase highEntropyPassphrase = new HighEntropyPassphrase();

        assertTrue(highEntropyPassphrase.validate("aa bb cc dd ee"));
        assertTrue(highEntropyPassphrase.validate("aa bb cc dd aaa"));
    }    @Test
    public void shouldNotValidPassphrase(){
        HighEntropyPassphrase highEntropyPassphrase = new HighEntropyPassphrase();

        assertFalse(highEntropyPassphrase.validate("aa bb cc dd aa"));

    }

    @Test
    public void shouldValidPassphrasePart2(){
        HighEntropyPassphrase highEntropyPassphrase = new HighEntropyPassphrase();

        assertTrue(highEntropyPassphrase.validate("abcde fghij"));
        assertTrue(highEntropyPassphrase.validate("a ab abc abd abf abj"));
        assertTrue(highEntropyPassphrase.validate("iiii oiii ooii oooi oooo"));
    }

    @Test
    public void shouldNotValidPassphrasePart2(){
        HighEntropyPassphrase highEntropyPassphrase = new HighEntropyPassphrase();

        assertFalse(highEntropyPassphrase.validate("abcde xyz ecdab"));
        assertFalse(highEntropyPassphrase.validate("oiii ioii iioi iiio"));

    }

}