package Day5AMazeofTwistyTrampolines;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class MazeOfTwistyTrampolinesTest {
    @Test
    public void shouldCreateObjectFromInputFile(){
        MazeOfTwistyTrampolines maze = new MazeOfTwistyTrampolines("Day5DataTest");

        List<Integer> expected = Arrays.asList(0,3,0,1,-3);

        assertEquals(expected, maze.getJumpList());
    }

    @Test
    public void shouldTake5StepsToEscapeMaze(){
        MazeOfTwistyTrampolines maze = new MazeOfTwistyTrampolines("Day5DataTest");

        List<Integer> expectedJumpList = Arrays.asList(2,5,0,1,-2);
        int expectedSteps = 5;

        assertEquals(expectedSteps, maze.stepsToEscape());
        assertEquals(expectedJumpList, maze.getJumpList());
    }

    @Test
    public void shouldTake10StepsToEscapeMaze(){
        MazeOfTwistyTrampolines maze = new MazeOfTwistyTrampolines("Day5DataTest");

        List<Integer> expectedJumpList = Arrays.asList(2,3,2,3,-1);
        int expectedSteps = 10;

        assertEquals(expectedSteps, maze.stepsToEscapeVer2());
        assertEquals(expectedJumpList, maze.getJumpList());
    }

}