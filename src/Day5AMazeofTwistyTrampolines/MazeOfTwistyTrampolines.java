package Day5AMazeofTwistyTrampolines;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class MazeOfTwistyTrampolines {
    private List<Integer> jumpList = new LinkedList<>();

    public MazeOfTwistyTrampolines(String fileName) {
        String line;
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            while(true){
                line = reader.readLine();
                if (line == null) break;
                jumpList.add(Integer.parseInt(line));
                //System.out.println(Integer.parseInt(line));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Integer> getJumpList() {
        return jumpList;
    }

    public int stepsToEscape() {
        int offset = 0;
        int jump;
        int steps = 0;

        while(offset >= 0 && offset < jumpList.size()){
            jump = jumpList.get(offset);
            jumpList.set(offset, jump + 1);
            offset += jump;
            steps++;
        }
        return steps;
    }

    public int stepsToEscapeVer2() {
        int offset = 0;
        int jump;
        int steps = 0;

        while(offset >= 0 && offset < jumpList.size()){
            jump = jumpList.get(offset);
            if(jump >= 3) jumpList.set(offset, jump - 1);
            else jumpList.set(offset, jump + 1);
            offset += jump;
            steps++;
        }
        return steps;
    }

    public static void main(String[] args) {
        MazeOfTwistyTrampolines maze = new MazeOfTwistyTrampolines("Day5DataPart1");

        System.out.println(maze.stepsToEscapeVer2());
    }
}
