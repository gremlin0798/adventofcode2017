package Day6MemoryReallocation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class MemoryReallocation {
    private List<Integer> memoryBanksList = new ArrayList<>();
    private Set<String> stateSet = new HashSet<>();

    public MemoryReallocation(String fileName) {
        String line;
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            while(true){
                line = reader.readLine();
                if(line == null) break;
                String[] lineSplit = line.split(" ");
                for(String number : lineSplit){
                    memoryBanksList.add(Integer.parseInt(number));
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Integer> getMemoryBanksList() {
        return memoryBanksList;
    }

    public void reallocateBanks(){
        int maxBlock = memoryBanksList.stream().max(Comparator.comparing(Integer::valueOf)).get();
        int maxBlockIndex = memoryBanksList.indexOf(maxBlock);
        int i = maxBlockIndex +1;
        memoryBanksList.set(maxBlockIndex, 0);
        while(maxBlock != 0){
            if(i >= memoryBanksList.size()) i = 0;
            memoryBanksList.set(i, memoryBanksList.get(i) + 1);
            i++;
            maxBlock--;
        }
    }

    public int countCycles(){
        int cycles = 0;
        resetStateSet();

        while (!detectLoop()){
            reallocateBanks();
            cycles++;
        }

        return cycles;
    }

    private boolean detectLoop(){
        if(stateSet.contains(memoryBanksList.toString())) return true;
        else stateSet.add(memoryBanksList.toString());
        return false;
    }

    public void resetStateSet(){
        stateSet = new HashSet<>();
    }

    public static void main(String[] args) {
        MemoryReallocation memoryReallocation = new MemoryReallocation("Day6DataPart1");
        System.out.println(memoryReallocation.countCycles());
        System.out.println(memoryReallocation.countCycles());
    }
}
