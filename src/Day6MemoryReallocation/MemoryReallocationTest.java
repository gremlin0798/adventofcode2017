package Day6MemoryReallocation;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class MemoryReallocationTest {

    @Test
    public void shouldCreateObjectFromInputFile(){
        MemoryReallocation memoryReallocation = new MemoryReallocation("Day6DataTest");

        List<Integer> expected = Arrays.asList(0,2,7,0);
        assertEquals(expected, memoryReallocation.getMemoryBanksList());
    }

    @Test
    public void shouldReallocateMemory(){
        MemoryReallocation memoryReallocation = new MemoryReallocation("Day6DataTest");

        List<Integer> expected = Arrays.asList(2,4,1,2);
        memoryReallocation.reallocateBanks();
        assertEquals(expected, memoryReallocation.getMemoryBanksList());

        expected = Arrays.asList(3,1,2,3);
        memoryReallocation.reallocateBanks();
        assertEquals(expected, memoryReallocation.getMemoryBanksList());

        expected = Arrays.asList(0,2,3,4);
        memoryReallocation.reallocateBanks();
        assertEquals(expected, memoryReallocation.getMemoryBanksList());

        expected = Arrays.asList(1,3,4,1);
        memoryReallocation.reallocateBanks();
        assertEquals(expected, memoryReallocation.getMemoryBanksList());

        expected = Arrays.asList(2,4,1,2);
        memoryReallocation.reallocateBanks();
        assertEquals(expected, memoryReallocation.getMemoryBanksList());
    }

    @Test
    public void shouldDetectLoopAndReturn5Cycles(){
        MemoryReallocation memoryReallocation = new MemoryReallocation("Day6DataTest");

        assertEquals(5, memoryReallocation.countCycles());
    }

    @Test
    public void shouldDetectLoopSecondTimeAfter4Cycles(){
        MemoryReallocation memoryReallocation = new MemoryReallocation("Day6DataTest");
        memoryReallocation.countCycles();
        assertEquals(4, memoryReallocation.countCycles());
    }

}