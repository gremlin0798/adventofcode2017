package Day8IHeardYouLikeRegisters;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RegisterInstructionTest {

    @Test
    public void shouldDecodeInstructions(){
        RegisterInstruction registerInstruction = new RegisterInstruction("s");

        Instruction expected = new Instruction("a",
                1,
                1,
                "b",
                 Instruction.Condition.LESS,
                5);
        assertEquals(expected, registerInstruction.decodeInstruction("a inc 1 if b < 5"));

        expected = new Instruction("asd",
                1,
                1,
                "b",
                Instruction.Condition.LESS_OR_EQUAL,
                5);
        assertEquals(expected, registerInstruction.decodeInstruction("asd inc 1 if b <= 5"));

        expected = new Instruction("b",
                1,
                5,
                "a",
                Instruction.Condition.GRATER,
                1);
        assertEquals(expected, registerInstruction.decodeInstruction("b inc 5 if a > 1"));

        expected = new Instruction("c",
                -1,
                -10,
                "a",
                Instruction.Condition.GRATER_OR_EQUAL,
                1);
        assertEquals(expected, registerInstruction.decodeInstruction("c dec -10 if a >= 1"));

        expected = new Instruction("c",
                1,
                -20,
                "c",
                Instruction.Condition.EQUAL,
                10);
        assertEquals(expected, registerInstruction.decodeInstruction("c inc -20 if c == 10"));

        expected = new Instruction("c",
                1,
                -20,
                "c",
                Instruction.Condition.NOT_EQUAL,
                10);
        assertEquals(expected, registerInstruction.decodeInstruction("c inc -20 if c != 10"));
    }

    @Test
    public void shouldReturnLargestValue1(){
        RegisterInstruction registerInstruction = new RegisterInstruction("Day8DataTest");

        registerInstruction.computeInstructions();
        assertEquals(java.util.Optional.of(1), java.util.Optional.ofNullable(registerInstruction.getLargestValueInRegisters()));
    }

}