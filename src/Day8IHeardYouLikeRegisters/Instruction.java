package Day8IHeardYouLikeRegisters;

public class Instruction {
    String registerToModify;
    int instructionType;
    int registerValue;
    String registerToCondition;

    public enum Condition {
        LESS,
        LESS_OR_EQUAL,
        EQUAL,
        NOT_EQUAL,
        GRATER,
        GRATER_OR_EQUAL

    }

    Condition condition;

    int valueToCondition;

    public Instruction(String registerToModify, int instructionType, int registerValue, String registerToCondition, String condition, int valueToCondition) {
        this.registerToModify = registerToModify;
        this.instructionType = instructionType;
        this.registerValue = registerValue;
        this.registerToCondition = registerToCondition;
        this.condition = getCondition(condition);
        this.valueToCondition = valueToCondition;
    }

    public Instruction(String registerToModify, int instructionType, int registerValue, String registerToCondition, Condition condition, int valueToCondition) {
        this.registerToModify = registerToModify;
        this.instructionType = instructionType;
        this.registerValue = registerValue;
        this.registerToCondition = registerToCondition;
        this.condition = condition;
        this.valueToCondition = valueToCondition;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj instanceof Instruction){
            Instruction otherInstruction = (Instruction) obj;
            return registerToModify.equals(otherInstruction.registerToModify) &&
                    instructionType == otherInstruction.instructionType &&
                    registerValue == otherInstruction.registerValue &&
                    registerToCondition.equals(otherInstruction.registerToCondition) &&
                    condition.equals(otherInstruction.condition) &&
                    valueToCondition == otherInstruction.valueToCondition;
        }
        return false;
    }

    public Condition getCondition(String condition){
        switch (condition) {
            case "<":
                return Condition.LESS;
            case "<=":
                return Condition.LESS_OR_EQUAL;
            case ">":
                return Condition.GRATER;
            case ">=":
                return Condition.GRATER_OR_EQUAL;
            case "==":
                return Condition.EQUAL;
            case "!=":
                return Condition.NOT_EQUAL;
            default:
                return null;
        }
    }
}
