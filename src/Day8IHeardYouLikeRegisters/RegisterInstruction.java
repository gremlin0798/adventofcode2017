package Day8IHeardYouLikeRegisters;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterInstruction {
    String fileName;
    Map<String, Integer> registersMap = new HashMap<>();
    Pattern pattern = Pattern.compile("(?<registerToModify>[a-z]+) (?<instructionType>inc|dec) (?<registerValue>-?\\d+) " +
            "if (?<registerToCondition>[a-z]+) (?<condition><|<=|>|>=|==|!=) (?<valueToCondition>-?\\d+)");
    Integer highestValueEver = Integer.MIN_VALUE;



    public RegisterInstruction(String fileName) {
        this.fileName = fileName;
    }

    public Integer getHighestValueEver() {
        return highestValueEver;
    }

    public void computeInstructions(){
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            String line;
            while(true){
                line = reader.readLine();
                if(line == null) break;
                System.out.println(line);
                Instruction instruction = decodeInstruction(line);
                if(checkCondition(instruction)) performInstruction(instruction);
                this.highestValueEver = getLargestValueInRegisters() > this.highestValueEver ?  getLargestValueInRegisters() : this.highestValueEver;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Instruction decodeInstruction(String line){
        Instruction instruction = null;

        Matcher matcher = pattern.matcher(line);

        if(matcher.matches()){
            String registerToModify = matcher.group("registerToModify");
            int instructionType = matcher.group("instructionType").equals("inc") ? 1 : -1;
            int registerValue = Integer.parseInt(matcher.group("registerValue"));
            String registerToCondition = matcher.group("registerToCondition");
            String condition = matcher.group("condition");
            int valueToCondition = Integer.parseInt(matcher.group("valueToCondition"));

            instruction = new Instruction(registerToModify, instructionType, registerValue, registerToCondition, condition, valueToCondition);
        }
        return instruction;
    }

    private Integer getRegisterValue(String register){
        if(registersMap.containsKey(register))
            return registersMap.get(register);
        else registersMap.put(register, 0);
        return 0;
    }

    private void modifyRegisterValue(String register, Integer value){
        if(registersMap.containsKey(register)) registersMap.put(register, registersMap.get(register) + value);
        else registersMap.put(register, value);
    }

    public Integer getLargestValueInRegisters(){
        return registersMap.entrySet().stream().map(r -> r.getValue()).max(Comparator.comparing(Integer::intValue)).get();
    }

    private boolean checkCondition(Instruction instruction){
        switch (instruction.condition){
            case LESS:
                return getRegisterValue(instruction.registerToCondition) < instruction.valueToCondition;
            case LESS_OR_EQUAL:
                return getRegisterValue(instruction.registerToCondition) <= instruction.valueToCondition;
            case GRATER:
                return getRegisterValue(instruction.registerToCondition) > instruction.valueToCondition;
            case GRATER_OR_EQUAL:
                return getRegisterValue(instruction.registerToCondition) >= instruction.valueToCondition;
            case EQUAL:
                return getRegisterValue(instruction.registerToCondition).equals(instruction.valueToCondition);
            case NOT_EQUAL:
                return !getRegisterValue(instruction.registerToCondition).equals(instruction.valueToCondition);
            default:
                return  false;
        }

    }

    private void performInstruction(Instruction instruction){
        modifyRegisterValue(instruction.registerToModify, instruction.instructionType * instruction.registerValue);
    }

    public static void main(String[] args) {
        RegisterInstruction registerInstruction = new RegisterInstruction("Day8DataPart1");

        registerInstruction.computeInstructions();
        System.out.println(registerInstruction.getLargestValueInRegisters());
        System.out.println(registerInstruction.getHighestValueEver());
    }
}
