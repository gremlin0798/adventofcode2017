package Day1InverseCaptcha;

import org.junit.Test;

import static org.junit.Assert.*;

public class InverseCaptchaTest {
    @Test
    public void shouldReturn3From1122(){
        InverseCaptcha inverseCaptcha = new InverseCaptcha();
        int expected = 3;

        assertEquals(expected, inverseCaptcha.solve("1122"));

    }

    @Test
    public void shouldReturn4From1111(){
        InverseCaptcha inverseCaptcha = new InverseCaptcha();
        int expected = 4;

        assertEquals(expected, inverseCaptcha.solve("1111"));
    }

    @Test
    public void shouldReturn0From1234(){
        InverseCaptcha inverseCaptcha = new InverseCaptcha();
        int expected = 0;

        assertEquals(expected, inverseCaptcha.solve("1234"));
    }

    @Test
    public void shouldReturn9From91212129(){
        InverseCaptcha inverseCaptcha = new InverseCaptcha();
        int expected = 9;

        assertEquals(expected, inverseCaptcha.solve("91212129") );
    }

    @Test
    public void solverVer2shouldReturn6From1212(){
        InverseCaptcha inverseCaptcha = new InverseCaptcha();
        int expected = 6;

        assertEquals(expected, inverseCaptcha.solveVer2("1212") );
    }

    @Test
    public void solverVer2shouldReturn0From1221(){
        InverseCaptcha inverseCaptcha = new InverseCaptcha();
        int expected = 0;

        assertEquals(expected, inverseCaptcha.solveVer2("1221") );
    }

    @Test
    public void solverVer2shouldReturn4From123425(){
        InverseCaptcha inverseCaptcha = new InverseCaptcha();
        int expected =4;

        assertEquals(expected, inverseCaptcha.solveVer2("123425") );
    }

    @Test
    public void solverVer2shouldReturn12From123123(){
        InverseCaptcha inverseCaptcha = new InverseCaptcha();
        int expected = 12;

        assertEquals(expected, inverseCaptcha.solveVer2("123123") );
    }

    @Test
    public void solverVer2shouldReturn4From12131415(){
        InverseCaptcha inverseCaptcha = new InverseCaptcha();
        int expected = 4;

        assertEquals(expected, inverseCaptcha.solveVer2("12131415") );
    }



}