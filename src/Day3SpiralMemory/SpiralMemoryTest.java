package Day3SpiralMemory;

import org.junit.Test;

import static org.junit.Assert.*;

public class SpiralMemoryTest {
    @Test
    public void shouldReturnShellNumber(){
        SpiralMemory spiralMemory = new SpiralMemory();

        assertEquals(0, spiralMemory.getShellNumber(1));
        assertEquals(2, spiralMemory.getShellNumber(12));
        assertEquals(2, spiralMemory.getShellNumber(23));
        assertEquals(16, spiralMemory.getShellNumber(1024));
    }

    @Test
    public void shouldReturnSideOfShell(){
        SpiralMemory spiralMemory = new SpiralMemory();

        int dataIdentifier = 1;
        assertEquals(0, spiralMemory.getSide(dataIdentifier, spiralMemory.getShellNumber(dataIdentifier)));
        dataIdentifier = 12;
        assertEquals(1, spiralMemory.getSide(dataIdentifier, spiralMemory.getShellNumber(dataIdentifier)));
        dataIdentifier = 23;
        assertEquals(4, spiralMemory.getSide(dataIdentifier, spiralMemory.getShellNumber(dataIdentifier)));
        dataIdentifier = 1024;
        assertEquals(2, spiralMemory.getSide(dataIdentifier, spiralMemory.getShellNumber(dataIdentifier)));
    }

    @Test
    public void shouldReturnStepsQuantity(){
        SpiralMemory spiralMemory = new SpiralMemory();

        int dataIdentifier = 1;
        assertEquals(0, spiralMemory.getStepsQuantity(dataIdentifier));

        dataIdentifier = 11;
        assertEquals(2, spiralMemory.getStepsQuantity(dataIdentifier));
        dataIdentifier = 15;
        assertEquals(2, spiralMemory.getStepsQuantity(dataIdentifier));
        dataIdentifier = 19;
        assertEquals(2, spiralMemory.getStepsQuantity(dataIdentifier));
        dataIdentifier = 23;
        assertEquals(2, spiralMemory.getStepsQuantity(dataIdentifier));

        dataIdentifier = 12;
        assertEquals(3, spiralMemory.getStepsQuantity(dataIdentifier));
        dataIdentifier = 1014;
        assertEquals(31, spiralMemory.getStepsQuantity(dataIdentifier));
    }
}