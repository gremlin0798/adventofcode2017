package Day3SpiralMemory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class SpiralMemoryPart2 {
    public void getIndex(int number){
        try(BufferedReader reader = new BufferedReader(new FileReader("Day3DataPart2"))){
            String line;
            String[] lineSplit;
            while(true){
                line = reader.readLine();
                if(line == null) break;
                lineSplit = line.split(" ");

                int index = Integer.parseInt(lineSplit[0]);
                int squareSum = Integer.parseInt(lineSplit[1]);

                if(number < squareSum) {System.out.println(index);
                                        break;}
            }



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SpiralMemoryPart2 spiralMemoryPart2 = new SpiralMemoryPart2();

        spiralMemoryPart2.getIndex(361527);
    }
}
