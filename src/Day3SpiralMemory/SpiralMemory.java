package Day3SpiralMemory;

/**
 * Day 3: Spiral Memory
 */

//TODO: Write Generator for spiral memory :(
public class SpiralMemory {
    /**
     * Every shell have i it's bottom right corner value of squared size. By comparing max available number with
     * looking data Identifier shell number is found.
     *
     * @param dataIdentifier
     * @return  shell Number
     */
    public int getShellNumber(int dataIdentifier) {
        int shellNumber = -1;

        for(int i = 0 ; true ; i++ ){
            int size = 2 * i + 1;
            int maxNumber = size*size;

            if(dataIdentifier <= maxNumber) {shellNumber = i; break;}
        }

        return shellNumber;
    }

    public int getSide(int dataIdentifier, int shellNumber){
        if(dataIdentifier == 1) return 0;

        int sideNumber = -1;
        int shellSize = 2 * shellNumber + 1;
        int dataIdentifierNew = dataIdentifier - (shellSize-2)*(shellSize-2);

        if(dataIdentifierNew <= (shellSize -1)) return 1;
        if(dataIdentifierNew <= 2*(shellSize-1)) return 2;
        if(dataIdentifierNew <= 3*(shellSize-1)) return 3;
        if(dataIdentifierNew <= 4*(shellSize-1)) return 4;

        return sideNumber ;
    }

    public int getStepsQuantity(int dataIdentifier){
        if(dataIdentifier == 1) return 0;
        int shellNumber = this.getShellNumber(dataIdentifier);
        int sideNumber = this.getSide(dataIdentifier, shellNumber);

        int shellSize = 2 * shellNumber + 1;
        int dataIdentifierNew = dataIdentifier - (shellSize-2)*(shellSize-2);

        int x = 0;
        int y = 0;

        switch (sideNumber){

            case 1: x = shellNumber;
                y = dataIdentifierNew - shellNumber;
                break;
            case 2: x = dataIdentifierNew - 3*shellNumber;
                y = shellNumber;
                break;
            case 3: x = -shellNumber;
                y = dataIdentifierNew + -5*shellNumber;
                break;
            case 4: x = dataIdentifierNew - 7*shellNumber;
                y = -shellNumber;
                break;
        }

        return Math.abs(x) + Math.abs(y);
    }

    public static void main(String[] args) {
       /* for(int i = 0 ; i < 20 ; i++ ){
            int size = 2 * i + 1;
            int maxNumber = size*size;

           System.out.println(i + " " + size + " " + maxNumber);
        }*/

       SpiralMemory spiralMemory = new SpiralMemory();
       System.out.println(spiralMemory.getStepsQuantity(361527));
       System.out.println(spiralMemory.getStepsQuantity(64));

    }
}
