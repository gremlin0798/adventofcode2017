package Day7RecursiveCircus;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("SpellCheckingInspection")
public class Program {
    private String name;
    private int weight;
    private Set<String> childrenSet = new HashSet<>();
    private int totalWeight;

    public Program(String name, int weight, String[] children) {
        this.name = name;
        this.weight = weight;
        this.totalWeight = 0;
        if(children != null) Arrays.stream(children).forEach(c -> childrenSet.add(c));
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public Set<String> getChildrenSet() {
        return childrenSet;
    }

    public int getTotalWeight() {
        return totalWeight;
    }

    @Override
    public String toString() {
        return name + "(" + weight + ") c:("
                + childrenSet.stream().map(c -> c + " ").reduce("",String::concat) + ")"
                + " t:" + totalWeight;

    }

    public boolean haveChildren(String children){
        return childrenSet.contains(children);
    }

    public int updateTotalWeight(Map<String, Program> nameProgramMap){
        this.totalWeight = weight;

        return this.totalWeight +=
                childrenSet.stream().map(c -> nameProgramMap.get(c)).map(p -> p.updateTotalWeight(nameProgramMap)).mapToInt(Integer::intValue).sum();

    }

    public Program getUnbalancedStack(Map<String, Program> nameProgramMap){

        List<Program> childrenSetSorted = childrenSet.stream()
                .map(c -> nameProgramMap.get(c))
                .sorted(Comparator.comparing(p -> p.getTotalWeight()))
                .collect(Collectors.toList());

        if(childrenSetSorted.get(0).getTotalWeight() != childrenSetSorted.get(1).getTotalWeight())
            return childrenSetSorted.get(0);
        if(childrenSetSorted.get(childrenSetSorted.size()-1).getTotalWeight() != childrenSetSorted.get(childrenSetSorted.size()-2).getTotalWeight())
            return childrenSetSorted.get(childrenSetSorted.size()-1);

        return null;
    }

    public static void main(String[] args) {
        Program program = new Program("sdsd", 45, new String[]{"fwercvwe", "sdsdf", "sdfsdf"});

        System.out.println(program.toString());
    }
}
