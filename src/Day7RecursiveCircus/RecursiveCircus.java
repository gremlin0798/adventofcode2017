package Day7RecursiveCircus;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RecursiveCircus {
    private List<Program> programList = new LinkedList<>();
    Pattern pattern = Pattern.compile("(\\w+)\\s*\\((\\d+)\\)\\s*");
    private Map<String, Program> nameProgramMap = new HashMap<>();

    public RecursiveCircus(String fileName) {
        loadProgramsTree(fileName);

        programList.stream().forEach(p -> nameProgramMap.put(p.getName(), p));
    }

    public List<Program> getProgramList() {
        return programList;
    }

    private void loadProgramsTree(String fileName){
        Matcher matcher;
        String line;
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            while (true) {
                line = reader.readLine();
                if (line == null) break;
                String[] parts = line.split(" -> ");
                String[] children = parts.length > 1 ? parts[1].split("\\s*,\\s*") : null;

                matcher = pattern.matcher(parts[0]);
                matcher.matches();

                String name = matcher.group(1);
                int weight = Integer.parseInt(matcher.group(2));

                programList.add(new Program(name, weight, children));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Program findRoot(){
        Program root = programList.get(new Random().nextInt(programList.size()-1));

            for(Program program : programList){
                if(program.haveChildren(root.getName())) root = findRoot();
            }

        return root;
    }

    public Program findUnbalancedStack(Program root){
        Program lastRoot = null;
        while (root != null){
            lastRoot = root;
            root = root.getUnbalancedStack(nameProgramMap);
        }

        return lastRoot;
    }

    public int findWeightToBalanceStack(Program unbalanced){
        Program parent = null;
        for(Program program : programList){
            if(program.haveChildren(unbalanced.getName())) parent = program;
        }

        List<Program> childrenMappedToProgram = parent.getChildrenSet().stream()
                .map(c -> nameProgramMap.get(c))
                .collect(Collectors.toList());

        for(Program program : childrenMappedToProgram){
            if(unbalanced.getTotalWeight() != program.getTotalWeight()) return unbalanced.getTotalWeight() - program.getTotalWeight();
        }
        return -1;
    }


    public static void main(String[] args) {
        RecursiveCircus recursiveCircus = new RecursiveCircus("Day7DataPart1");


        Program root = recursiveCircus.findRoot();
        System.out.println(root);
        root.updateTotalWeight(recursiveCircus.nameProgramMap);
        recursiveCircus.getProgramList().forEach(System.out::println);
        System.out.println(root);

        System.out.println(recursiveCircus.findUnbalancedStack(root));
        System.out.println(recursiveCircus.findWeightToBalanceStack(recursiveCircus.findUnbalancedStack(root)));

    }



}
